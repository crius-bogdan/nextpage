import Vue from 'vue'
import App from './App.vue'
import store from './store'
import "@/assets/scss/main.scss"
import VueScrollmagic from 'vue-scrollmagic'
Vue.config.productionTip = false;

Vue.use(VueScrollmagic)

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
